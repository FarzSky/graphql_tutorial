import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// components
import BookList from './components/BookList';
import AddBook from './components/AddBook';

// apollo client setup
const apolloClient = new ApolloClient({
    uri: "http://localhost:4000/graphql"
});

function App() {
    return (
        <ApolloProvider client={apolloClient}>
            <div id="main">
                <h1>Ninja reading list</h1>
                <BookList />
                <br />
                <br />
                <AddBook />
            </div>
        </ApolloProvider>
    );
}

export default App;
